#!/usr/bin/python
# Contar las palabras del archivo mobidick.txt 
# key -> palabra , clave
# Recibe informacion de la clave estandar, procesa la informacion
# y lo que se emite va a la salida estandar (imprimir key,value)
# El mismo delimitador (,) usarlo en reducer

import sys

for line in sys.stdin:
 words = line.split(' ')
 line = line.strip()	
	
 for w in words:
  w = w.replace(','," ")
  w = w.replace('.'," ")
  w = w.replace(';'," ")
  print "%s,%s" % (w,'1')

#cat mobibidick | python mapper.py

