Se realizaron pruebas de IO-bound con archivos de diferentes tamaño (grandes y pequeños).

Para las pruebas se utilizó un script que cuenta las ocurrencias de todas las palabras de un archivo que se le de de entrada.
Este archivo es el mapper.py y el archivo que se le envió a la entrada es el denominado mobidick.txt.

Se utilizó el comando "time" junto con el comando br de BashReduce para tomar el tiempo que toma:

time ./br -m "localhost" -r /home/andrea/Repositorios/bashreduce/mapper.py -i /home/andrea/Repositorios/bashreduce/bigfile4.txt -o salida.txt

Y se pobró con 1,2,3 y 4 cores.

